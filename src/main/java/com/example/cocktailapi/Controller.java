package com.example.cocktailapi;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private ImageView imageView;
    @FXML
    private ListView<Coctel> listViewCocteles;
    @FXML
    private ChoiceBox<Categoria> choiceCategoria;
    @FXML
    private Button btAleatorio, btCategoria, btBuscar;
    @FXML
    private TextField txtBuscarNombre;

    @FXML
    private Button btAleatorioAlcoholico;

    @FXML
    private Button btAleatorioNoAlcoholico;

    @FXML
    private Button btReset;

    private ObservableList<Categoria> listaCategorias;
    private ObservableList<Coctel> listaCocteles;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listaCategorias = FXCollections.observableArrayList();
        listaCocteles = FXCollections.observableArrayList();
        try {
            jsonCategorias();
        } catch (IOException e) {
            e.printStackTrace();
        }
        asociarElementos();

        listViewCocteles.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        mostrarImagen(((Coctel) newValue).getImagenUrl());
                    }
                }
        );
        btAleatorioAlcoholico.setOnAction(this::generarCoctelAleatorioAlcoholico);
        btAleatorioNoAlcoholico.setOnAction(this::generarCoctelAleatorioNoAlcoholico);
        btReset.setOnAction(this::limpiarLista);
    }


    public void generarCoctelCategoria(ActionEvent actionEvent) {
        if (choiceCategoria.getSelectionModel().getSelectedIndex() > -1) {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" +
                    choiceCategoria.getSelectionModel().getSelectedItem().getNombre();
            try {
                obtenerCoctelAleatorioPorCategoria(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("No hay categoría seleccionada");
        }
    }

    private void obtenerCoctelAleatorioPorCategoria(String url) throws IOException {
        InputStream inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");

        if (jsonArray != null && jsonArray.length() > 0) {
            int indiceAleatorio = (int) (Math.random() * jsonArray.length());
            String coctelNombre = jsonArray.getJSONObject(indiceAleatorio).getString("strDrink");
            String imagenUrl = jsonArray.getJSONObject(indiceAleatorio).getString("strDrinkThumb");
            listaCocteles.add(new Coctel(coctelNombre, imagenUrl));
            mostrarImagen(imagenUrl);
        } else {
            System.out.println("No se encontraron cócteles");
        }
    }

    public void generarCoctelAleatorio(ActionEvent actionEvent) {
        String url = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
        try {
            obtenerCocteles(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generarCoctelAleatorioAlcoholico(ActionEvent actionEvent) {
        try {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic";
            obtenerCoctelAleatorio(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generarCoctelAleatorioNoAlcoholico(ActionEvent actionEvent) {
        try {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic";
            obtenerCoctelAleatorio(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void obtenerCoctelAleatorio(String url) throws IOException {
        InputStream inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");

        if (jsonArray != null && jsonArray.length() > 0) {
            int indiceAleatorio = (int) (Math.random() * jsonArray.length());
            String coctelNombre = jsonArray.getJSONObject(indiceAleatorio).getString("strDrink");
            String imagenUrl = jsonArray.getJSONObject(indiceAleatorio).getString("strDrinkThumb");
            listaCocteles.add(new Coctel(coctelNombre, imagenUrl));
            mostrarImagen(imagenUrl);
        } else {
            System.out.println("No se encontraron cócteles");
        }
    }


    @FXML
    public void buscarCocktailPorNombre(ActionEvent actionEvent) {
        String nombre = txtBuscarNombre.getText();
        if (!nombre.isEmpty()) {
            listaCocteles.clear();
            String url = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + nombre;
            try {
                obtenerCocteles(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Ingresa un nombre para buscar.");
        }
    }

    private void obtenerCocteles(String url) throws IOException {
        InputStream inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");

        if (jsonArray != null) {

            for (int i = 0; i < jsonArray.length(); i++) {
                String coctelNombre = jsonArray.getJSONObject(i).getString("strDrink");
                String imagenUrl = jsonArray.getJSONObject(i).getString("strDrinkThumb");
                listaCocteles.add(new Coctel(coctelNombre, imagenUrl));
            }


            if (!listaCocteles.isEmpty()) {
                mostrarImagen(listaCocteles.get(listaCocteles.size() - 1).getImagenUrl());
            }
        } else {
            System.out.println("No se encontraron cócteles");
        }
    }



    private void asociarElementos() {
        choiceCategoria.setItems(listaCategorias);
        listViewCocteles.setItems(listaCocteles);
    }

    private void jsonCategorias() throws IOException {
        String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
        InputStream inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");

        for (int i = 0; i < jsonArray.length(); i++) {
            String categoria = jsonArray.getJSONObject(i).getString("strCategory");
            listaCategorias.add(new Categoria(categoria));
        }
    }

    private void mostrarImagen(String imageUrl) {
        try {
            Image image = new Image(imageUrl);
            imageView.setImage(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void limpiarLista(ActionEvent actionEvent) {
        listaCocteles.clear();
        imageView.setImage(null);
    }
}
