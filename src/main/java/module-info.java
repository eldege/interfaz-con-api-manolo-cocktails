module com.example.cocktailapi {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;


    opens com.example.cocktailapi to javafx.fxml;
    exports com.example.cocktailapi;
}